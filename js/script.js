$(document).ready(function () {

    function changeClassActiveForLiTabs(element1, element2) {
        $(element1).on('click', 'li:not(.active)', function () {
            $(this)
                .addClass('active').siblings().removeClass('active')
                .closest('div.container').find(element2).removeClass('active').eq($(this).index()).addClass('active');
        });
    }



    // function for the restriction of number of images of some container that should be displayed after the page loading//

    function showImages(className, btnID, numToShow) {

        $("." + className).hide().siblings().hide();
        let imageList = $("." + className);
        let numInList = imageList.length;

        if (numInList > numToShow) {
            $("#" + btnID).show();
        }
        imageList.slice(0, numToShow).show();
        let nowShowing = imageList.filter(':visible').length;
        if (nowShowing >= numInList) {
            $("#" + btnID).hide();
        }
        return false;
    }


    // function for adding images by click #loadMoreBtn with a 2s delay//
    function addImages(className, btnName, numToAdd) {

        let imageList = $('.' + className);

        let numInList = imageList.length;

        let showing = imageList.filter(':visible').length;

        let preloader = $(btnName).closest('div.btn-container').find(".preloader");

        $(preloader).fadeIn(); // loading imitation
        setTimeout(function () {
            $(preloader).fadeOut();
            imageList.slice(showing - 1, showing + numToAdd).fadeIn();
            nowShowing = imageList.filter(':visible').length;

            if (nowShowing >= numInList) {
                $(btnName).hide();
            }
        }, 2000);
    }


    // function for applying masonry-plugin for images aligning//
    function alignByMasonry() {
        $('.image-gallery__images-container').imagesLoaded(function () {

            $('.image-gallery__images-container').masonry({
                itemSelector: '.img-box',
                gutter: 17,
            });
        });
    }




    // show/hide #navbarSearch field

    $('#navbarSearchIcon').on("click", function (event) {

        $('.navbar-content').css('width', '961px');
        $('#navbarSearch').fadeIn().focus();
        event.preventDefault();
    });
    $('#navbarSearch').blur(function () {

        $('#navbarSearch').hide();
        $('.navbar-content').css('width', '1115px');

    });





    //menu-top-link: scrolling to anchors

    $('a.menu-top-link').on("click", function (e) {
        let anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top - -10
        }, 800);
        e.preventDefault();
    });



    //section.services: tabs function//

    changeClassActiveForLiTabs('ul.services-tabs__caption', 'div.services-tabs__content');




    // .work-portfolio: adding images by clicking #loadMoreBtn1 and filtering by toolbar//

    showImages("tile", "loadMoreBtn1", 12); // set initial image shown


    // filtering
    $(".work-examples__toolbar li").click(function () {
        let selectedClass = $(this).attr("data-rel");
        $(".work-portfolio").fadeTo(100, 0.1);
        $(".work-portfolio div").not("." + selectedClass).removeClass('scale-anm');
        setTimeout(function () {
            $("." + selectedClass).addClass('scale-anm');
            $(".work-portfolio").fadeTo(300, 1);
        }, 300);
        setTimeout(function () {
            showImages(selectedClass, "loadMoreBtn1", 12);
        }, 320);
        $(this).addClass('active').siblings().removeClass('active');
    });

    // images adding
    $("#loadMoreBtn1").click(function () {

        addImages('tile', this, 12);

    });



    // cancel the default link of .news-link //

    $('.news-link').on("click", function (e) {

        e.preventDefault();
    });




    //#impressionsSection: Carousel Slider//


    // Carousel Slider //
    let elemPerPage = 4; // set number of elements per page in the list

    let elemWidth = (parseFloat($('.impressions__list_item').css('width')) + parseFloat($('.impressions__list_item').css('margin-right')) + parseFloat($('.impressions__list_item').css('margin-left'))); // get the width of one element in the list in pixels

    $('.impressions__list-wrapper').css('width', elemWidth * elemPerPage + 92 + 'px'); // set the widht of .list-wrapper in pixels according to the number of elements per page in the list 

    $('.impressions__list-box').css('width', elemWidth * elemPerPage + 'px'); // set the widht of ul.list-box in pixels according to the number of elements per page in the list        

    let totalElem = $('.impressions__list li').length; // Number of total elements
    let marginLeft = 0;
    let count = 0;

    let numSlides = Math.ceil(totalElem / elemPerPage); // Number of slides

    if (totalElem > elemPerPage) {
        $('.arrow.forward').css('background-color', '#18cfab');

        $('.arrow.back').on('click', function () { // Go back slider

            $('.arrow.forward').css('background-color', '#18cfab');

            if (marginLeft < 0) {

                count--;
                marginLeft = marginLeft + 100;

                $('.impressions__list').animate({
                    marginLeft: marginLeft + "%"
                }, 1500);

                if (marginLeft == 0) {
                    $('.arrow.back').css('background-color', 'transparent');
                }
            }
        });

        $('.arrow.forward').on('click', function () { // Go forward slider

            count++;
            $('.arrow.back').css('background-color', '#18cfab');

            if (count < numSlides) {

                if (marginLeft <= 0) {

                    marginLeft = marginLeft - 100;

                    $('.impressions__list').animate({
                        marginLeft: marginLeft + "%"
                    }, 1500);

                }
                if (count + 1 == numSlides) {
                    $('.arrow.forward').css('background-color', 'transparent');
                }

            } else {

                count--;
                $('.arrow.forward').css('background-color', 'transparent');

            }

        });

    }


    // Open infoBox //
    changeClassActiveForLiTabs('ul.impressions__list', 'li.impressions__info-box_item');




    // .image-gallery section: aligning with Masonry-plugin and adding elements on button clicking//

    alignByMasonry(); // apply Masonry aligning after page loading


    showImages("img-box", "loadMoreBtn2", 10); // set images initially shown


    $("#loadMoreBtn2").click(function () {

        addImages('img-box', this, 10);
        setTimeout(function () {
            alignByMasonry();
        }, 2000); // apply Masonry aligning after adding images

    });

    
// open/hide modal window //
    $('.modal-window').hide();

    $('.open-modal').on('click', function () {
        let getHref = $(this).attr('href');
        $(getHref).show(1000);
    });
    $('.submit-btn').on('click', function () {
        $('.modal-window').hide(1000);
    });
    $('.subscribeBtn').on('click', function () {
        $('.modal-window').hide(1000);
    });


    $(document).keyup(function (e) {
        if (e.keyCode == 27) {
            $('.modal-window').hide(1000);
        }
    });

});






//пример когда контейнер ресайзится в результате ресайза окна
/*jQuery(function ($) {
    function fix_size() {
        var images = $('.img-container img');
        images.each(setsize);

        function setsize() {
            var img = $(this),
                img_dom = img.get(0),
                container = img.parents('.img-container');
            if (img_dom.complete) {
                resize();
            } else img.one('load', resize);

            function resize() {
                if ((container.width() / container.height()) < (img_dom.width / img_dom.height)) {
                    img.width('100%');
                    img.height('auto');
                    return;
                }
                img.height('100%');
                img.width('auto');
            }
        }
    }
    $(window).on('resize', fix_size);
    fix_size();
});*/
